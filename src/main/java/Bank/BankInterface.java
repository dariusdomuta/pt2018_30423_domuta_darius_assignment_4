package Bank;

import Models.Account;
import Models.Person;
import Models.SavingsAccount;
import Models.SpendingAccount;

public interface BankInterface {

    /**
     * @pre isWellFormed(), CNP Size == 13, Cnp contains only digits, Account ID has 4 digits, person exists, sufficient funds
     * @post isWellFormed(), All persons have accounts, Jtable is not empty after delete
     */

    //CNP Size == 13, Cnp contains only digits
    public void addPerson(Person person);

    //CNP Size == 13, Cnp contains only digits
    public Person editPerson(Person person, String name, String cnp);

    //Jtable is not empty after delete
    public void deletePerson(Person person);

    //Jtable is not empty after delete
    public String[][] viewPersons();

    //Account ID has 4 digits, person exists
    public void addSavingsAccount(Person person, SavingsAccount savingsAccount);

    //Account ID has 4 digits, person exists
    public void addSpendingAccount(Person person, SpendingAccount spendingAccount);

    //Account ID has 4 digits, person exists
    public Account editAccount(Account account, int id, int balance);

    //Account ID has 4 digits, person exists, All persons have accounts
    public void deleteAccount(Account account);
    public String[][] viewAccounts();

    //sufficient funds
    public void deposit(int accountId, int amount);

    //funds > 0
    public void withdraw(int accountId, int amount);
}
