package Bank;

import Models.Account;
import Models.Person;
import Models.SavingsAccount;
import Models.SpendingAccount;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Bank implements BankInterface, Serializable {

    private HashMap<Person, ArrayList<Account>> bankDB;
    public Bank() {
        bankDB = new HashMap<Person, ArrayList<Account>>();
    }

    public HashMap<Person, ArrayList<Account>> getBankDB() {
        return bankDB;
    }

    public void setBankDB(HashMap<Person, ArrayList<Account>> bankDB) {
        this.bankDB = bankDB;
    }

    public boolean isWellFormed(){
        boolean ok = true;
        Set<Person> persons = bankDB.keySet();
        for (Person person : persons) {
            ArrayList<Account> accounts = bankDB.get(person);
            for (Account account : accounts) {
                if (account.getBalance() < 0) {
                    ok=false;
                }
            }
        }
        return ok;
    }

    @Override
    public void addPerson(Person person) {
        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
        assert person.getCnp().length() == 13 : "Invalid CNP Size!!!";

        int personsNumber = bankDB.size();

        if (person.getCnp().matches("[0-9]+") && person.getCnp().length() > 2) {
            bankDB.put(person, new ArrayList<Account>());
        } else {
            assert false : "CNP contains only numbers";
        }

        assert bankDB.size()==personsNumber+1 : "The nunmber of persons in the map was not incremented";
        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
    }

    @Override
    public Person editPerson(Person person, String name, String cnp) {
        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
        assert bankDB.containsKey(person) : "Person to edit not found";
        assert cnp.length() == 13 : "Invalid CNP Size!!!";

        if (person.getCnp().matches("[0-9]+") && person.getCnp().length() > 2) {
            ArrayList<Account> accountsCopy = bankDB.get(person);
            bankDB.remove(person);
            person.setName(name);
            person.setCnp(cnp);
            for (Account account: accountsCopy) {
                account.setOwner(person);
            }
            bankDB.put(person, accountsCopy);
        } else {
            assert false : "CNP contains only numbers";
        }
        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
        return person;
    }

    @Override
    public void deletePerson(Person person) {
        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
        assert bankDB.containsKey(person) : "Person to delete not found";

        bankDB.remove(person);

        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
    }

    @Override
    public String[][] viewPersons() {
        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
        assert !bankDB.isEmpty() : "The bank is empty!!!";
        String[][] personsData = new String[100][100];
        Set<Person> personSet = bankDB.keySet();
        int i = 0;
        int j = 0;

        for (Person person : personSet) {
            personsData[i][j] = person.getName();
            personsData[i][j+1] = person.getCnp();
            i++;
        }
        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
        return personsData;
    }

    @Override
    public void addSavingsAccount(Person person, SavingsAccount savingsAccount) {
        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
        assert bankDB.containsKey(person) : "There is no such person!!!";
        assert String.valueOf(savingsAccount.getId()).length() == 4 : "Invalid ID Size!!!";

        if (String.valueOf(savingsAccount.getId()).matches("[0-9]+") && person.getCnp().length() > 2) {
            ArrayList<Account> auxiliary = new ArrayList<>();

            auxiliary = bankDB.get(person);
            auxiliary.add(savingsAccount);
            bankDB.put(person, auxiliary);
        } else {
            assert false : "ID not well formed. Must contain 4 digits";
        }

        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
    }

    @Override
    public void addSpendingAccount(Person person, SpendingAccount spendingAccount) {
        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
        assert bankDB.containsKey(person) : "There is no such person!!!";
        assert String.valueOf(spendingAccount.getId()).length() == 4 : "Invalid ID Size!!!";

        if (String.valueOf(spendingAccount.getId()).matches("[0-9]+") && person.getCnp().length() > 2) {
            ArrayList<Account> auxiliary = new ArrayList<>();

            auxiliary = bankDB.get(person);

            auxiliary.add(spendingAccount);

            bankDB.put(person, auxiliary);
        } else {
            assert false : "ID not well formed. Must contain 4 digits";
        }

        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
    }

    @Override
    public Account editAccount(Account account, int id, int balance) {
        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
        assert bankDB.containsKey(account.getOwner()) : "there is no owner for this account";

        account.setId(id);
        account.setBalance(balance);

        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
        return account;
    }

    @Override
    public void deleteAccount(Account account) {
        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
        assert bankDB.containsKey(account.getOwner()) : "there is no owner for this accout";
        assert bankDB.get(account.getOwner()).contains(account) : "There is no such account!!";

        bankDB.get(account.getOwner()).remove(account);

        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
    }

    @Override
    public String[][] viewAccounts() {
        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";

        int ok = 0;
        String[][] personsData = new String[100][100];
        Set<Person> persons = bankDB.keySet();
        int i = 0;
        int j = 0;

        for (Person person : persons) {
            ArrayList<Account> accounts = bankDB.get(person);
            for (Account account : accounts) {
                int accountId = account.getId();
                String accountOwner = account.getOwner().getName();
                int accountBalance = account.getBalance();
                ok=1;
                personsData[i][j] = Integer.toString(accountId);
                personsData[i][j+1] = accountOwner;
                personsData[i][j+2] = Integer.toString(accountBalance);
                i++;
            }
        }

        assert  !(ok==0) : "There are no accounts!!!";
        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";

        return personsData;
    }

    @Override
    public void deposit(int accountId, int amount) {
        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
        int ok=0;
        Set<Person> personSet = bankDB.keySet();
        for (Person person: personSet) {
            ArrayList<Account> accounts = bankDB.get(person);
            for (Account account: accounts){
                if (account.getId() == accountId) {
                    ok =1;
                    account.deposit(amount);
                    System.out.println("successful deposit!!!");
                }
            }
        }
        assert !(ok==0): "There is no such account!!!";

        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
    }

    @Override
    public void withdraw(int accountId, int amount) {
        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
        int ok=0;
        Set<Person> personSet = bankDB.keySet();
        for (Person person: personSet) {
            ArrayList<Account> accounts = bankDB.get(person);
            for (Account account: accounts){
                if (account.getId() == accountId) {
                    ok =1;
                    account.withdraw(amount);
                }
            }
        }

        assert !(ok==0): "There is no such account!!!";
        assert isWellFormed() : "Not well formed, some account has balance less than 0!!!";
    }
}
