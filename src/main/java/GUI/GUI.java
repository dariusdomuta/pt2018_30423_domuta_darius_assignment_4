package GUI;

import Models.Account;
import Models.Person;
import Models.SavingsAccount;
import Models.SpendingAccount;
import com.sun.deploy.util.SessionState;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.util.ArrayList;
import java.util.Set;

import Bank.Bank;

    public class GUI extends JFrame{

        private static final long serialVersionUID = 1L;
        private JPanel panel;
        private int WIDTH = 1000, HEIGHT = 1000;
        private Bank bank = new Bank();

        public void readSerializable(Bank bank){
            try {
                FileOutputStream fileOut =
                        new FileOutputStream("C:\\Users\\dariu\\IdeaProjects\\Assignment4\\src\\main\\resources\\data.ser");
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(bank);
                out.close();
                fileOut.close();
                System.out.printf("Serialized data is saved in C:\\Users\\dariu\\IdeaProjects\\Assignment4\\src\\main\\resources\\data.ser");
            } catch (IOException i) {
                i.printStackTrace();
            }
        }

        public void writeDeserializable(Bank bank){
            try {
                FileInputStream fileIn = new FileInputStream("C:\\Users\\dariu\\IdeaProjects\\Assignment4\\src\\main\\resources\\data.ser");
                ObjectInputStream in = new ObjectInputStream(fileIn);
                bank = (Bank) in.readObject();
                in.close();
                fileIn.close();
            } catch (IOException i) {
                i.printStackTrace();
                return;
            } catch (ClassNotFoundException c) {
                System.out.println("Bank class not found");
                c.printStackTrace();
                return;
            }
        }

        public GUI() {
            panel = new JPanel();
            this.add(panel);
            panel.setLayout(null);
            this.setSize(WIDTH, HEIGHT);
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setLocationRelativeTo(null);
            this.setVisible(true);

            readSerializable(bank);


        }



        public void errorDisplay(String error) {
            JOptionPane.showMessageDialog(null, error, "Info", JOptionPane.INFORMATION_MESSAGE);
        }

        public void prepareDummyData() {
            Person person1 = new Person("Domuta Darius Spend", "1970724324781");
            Person person2 = new Person("Marcu Bogdan Save", "1970724324782");


            bank.addPerson(person1);
            bank.addPerson(person2);

            SpendingAccount spendingAccount = new SpendingAccount(1000, person1, 100, 0);
            SavingsAccount savingsAccount = new SavingsAccount(2000, person2, 200, 2, 2);


            bank.addSpendingAccount(person1, spendingAccount);
            bank.addSavingsAccount(person2, savingsAccount);
            writeDeserializable(bank);
            chooseRights();
        }


        public void chooseRights(){
            panel.removeAll();
            panel.revalidate();
            panel.setLayout(null);

            JButton AccountBt = new JButton("User");
            AccountBt.setBounds(40,40, 100, 40);
            panel.add(AccountBt);

            JButton adminBt = new JButton("Admin");
            adminBt.setBounds(40,120, 100, 40);
            panel.add(adminBt);

            AccountBt.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    userInterface();
                }
            });

            adminBt.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    adminInterface();
                }
            });
            writeDeserializable(bank);
            panel.repaint();
            panel.revalidate();
        }

        public void adminInterface(){
            panel.removeAll();
            panel.setLayout(null);

            JButton insertButton = new JButton("ADD PERSON");
            insertButton.setBounds(40, 40, 200, 30);
            panel.add(insertButton);

            JButton updateButton = new JButton("UPDATE PERSON");
            updateButton.setBounds(40, 80, 200, 30);
            panel.add(updateButton);

            JButton deleteButton = new JButton("DELETE PERSON");
            deleteButton.setBounds(40, 123, 200, 30);
            panel.add(deleteButton);

            JButton findALLButton = new JButton("SHOW ALL PERSONS");
            findALLButton.setBounds(40, 160, 200, 30);
            panel.add(findALLButton);

            JButton insertSavingsAccountButton = new JButton("ADD  SAVINGS ACCOUNT");
            insertSavingsAccountButton.setBounds(300, 40, 200, 30);
            panel.add(insertSavingsAccountButton);

            JButton insertSpendingAccountButton = new JButton("ADD SPENDING ACCOUNT");
            insertSpendingAccountButton.setBounds(300, 80, 200, 30);
            panel.add(insertSpendingAccountButton);

            JButton updateAccountButton = new JButton("UPDATE ACCOUNT");
            updateAccountButton.setBounds(300, 123, 200, 30);
            panel.add(updateAccountButton);

            JButton deleteAccountButton = new JButton("DELETE ACCOUNT");
            deleteAccountButton.setBounds(300, 160, 200, 30);
            panel.add(deleteAccountButton);

            JButton findALLAccountsButton = new JButton("SHOW ALL ACCOUNTS");
            findALLAccountsButton.setBounds(300, 200, 200, 30);
            panel.add(findALLAccountsButton);

            JButton backButton = new JButton("BACK");
            backButton.setBounds(40, 600, 200, 30);
            panel.add(backButton);

            backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    chooseRights();
                }
            });



            insertButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    insertPerson();
                }
            });

            updateButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    editPerson();
                }
            });

            deleteButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    deletePerson();
                }
            });

            findALLButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    showAllPersons();
                }
            });

            insertSavingsAccountButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    insertSavingsAccount();
                }
            });

            insertSpendingAccountButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    insertSpendingsAccount();
                }
            });

            updateAccountButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    editAccount();
                }
            });

            deleteAccountButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    deleteAccount();
                }
            });

            findALLAccountsButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    showAllAccounts();
                }
            });


            panel.repaint();
            panel.revalidate();

        }

        public void showAllPersons() {
            String[] columns = {"Name", "Cnp"};
            try {
                JTable jTable = new JTable(bank.viewPersons(),columns);
                JScrollPane scrollPane = new JScrollPane(jTable);
                scrollPane.setBounds(50,250,400,200);
                panel.add(scrollPane);
            } catch (AssertionError er) {
                errorDisplay(er.getMessage());
            }

            writeDeserializable(bank);
        }

        public void insertPerson(){
            panel.removeAll();
            panel.setLayout(null);

            JLabel nameLabel = new JLabel("name: ");
            nameLabel.setBounds(40, 40, 100, 30);
            panel.add(nameLabel);

            final JTextField nameTf = new JTextField();
            nameTf.setBounds(150, 40, 100, 30);
            panel.add(nameTf);

            JLabel cnpLabel = new JLabel("cnp: ");
            cnpLabel.setBounds(40, 80, 100, 30);
            panel.add(cnpLabel);

            final JTextField cnpTf = new JTextField();
            cnpTf.setBounds(150, 80, 100, 30);
            panel.add(cnpTf);

            JButton insertButton = new JButton("INSERT");
            insertButton.setBounds(40, 200, 100, 30);
            panel.add(insertButton);

            insertButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Person person = new Person(nameTf.getText(), cnpTf.getText());
                    try {
                        bank.addPerson(person);
                    } catch (AssertionError ex) {
                        errorDisplay(ex.getMessage());
                    }

                    System.out.println(person.hashCode());
                    adminInterface();
                }
            });

            panel.repaint();
            panel.revalidate();

            JButton backButton = new JButton("BACK");
            backButton.setBounds(40, 600, 200, 30);
            panel.add(backButton);

            backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    adminInterface();
                }
            });
            writeDeserializable(bank);
        }

        public void editPerson(){
            panel.removeAll();
            panel.setLayout(null);

            JLabel cnpLabel = new JLabel("old cnp: ");
            cnpLabel.setBounds(40, 40, 100, 30);
            panel.add(cnpLabel);

            final JTextField cnpTf = new JTextField();
            cnpTf.setBounds(150, 40, 100, 30);
            panel.add(cnpTf);

            JLabel newCnpLabel = new JLabel("new newCnp: ");
            newCnpLabel.setBounds(40, 80, 100, 30);
            panel.add(newCnpLabel);

            final JTextField newCnpTf = new JTextField();
            newCnpTf.setBounds(150, 80, 100, 30);
            panel.add(newCnpTf);

            JLabel newNameLabel = new JLabel("new Name: ");
            newNameLabel.setBounds(40, 120, 100, 30);
            panel.add(newNameLabel);

            final JTextField newNameTf = new JTextField();
            newNameTf.setBounds(150, 120, 100, 30);
            panel.add(newNameTf);

            JButton updateButton = new JButton("UPDATE");
            updateButton.setBounds(40, 200, 100, 30);
            panel.add(updateButton);

            updateButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        int ok = 0;
                        Set<Person> persons = bank.getBankDB().keySet();
                        for(Person person: persons) {
                            if(person.getCnp().equals(cnpTf.getText())) {
                                ok = 1;
                                bank.editPerson(person, newNameTf.getText(), newCnpTf.getText());
                            }
                        }
                        assert !(ok==0) : "Person to edit not found";

                    } catch (AssertionError er) {
                        errorDisplay(er.getMessage());
                    }
                    adminInterface();
                }
            });

            JButton backButton = new JButton("BACK");
            backButton.setBounds(40, 600, 200, 30);
            panel.add(backButton);

            backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    adminInterface();
                }
            });
            writeDeserializable(bank);
            panel.repaint();
            panel.revalidate();

        }

        public void deletePerson(){
            panel.removeAll();
            panel.setLayout(null);

            JLabel cnpLabel = new JLabel("cnp: ");
            cnpLabel.setBounds(40, 40, 100, 30);
            panel.add(cnpLabel);

            final JTextField cnpTf = new JTextField();
            cnpTf.setBounds(150, 40, 100, 30);
            panel.add(cnpTf);
            

            JButton deleteButton = new JButton("delete");
            deleteButton.setBounds(40, 200, 100, 30);
            panel.add(deleteButton);

            deleteButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int ok = 0;
                    Set<Person> persons = bank.getBankDB().keySet();
                    for(Person person: persons) {
                        if(person.getCnp().equals(cnpTf.getText())) {
                            try {
                                bank.deletePerson(person);
                                ok=1;
                            } catch (AssertionError er) {
                                errorDisplay(er.getMessage());
                            }

                        }
                    }
                    try {
                        assert ok!=0 : "There is no person with the given cnp!!!";
                    } catch (AssertionError er) {
                        errorDisplay(er.getMessage());
                    }

                    adminInterface();
                }
            });

            JButton backButton = new JButton("BACK");
            backButton.setBounds(40, 600, 200, 30);
            panel.add(backButton);

            backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    adminInterface();
                }
            });
            writeDeserializable(bank);
            panel.repaint();
            panel.revalidate();

        }

        public void insertSavingsAccount(){
            panel.removeAll();
            panel.setLayout(null);

            JLabel idLabel = new JLabel("id: ");
            idLabel.setBounds(40, 40, 100, 30);
            panel.add(idLabel);

            final JTextField idTf = new JTextField();
            idTf.setBounds(150, 40, 100, 30);
            panel.add(idTf);

            JLabel ownerNameLabel = new JLabel("owner name: ");
            ownerNameLabel.setBounds(40, 80, 100, 30);
            panel.add(ownerNameLabel);

            final JTextField ownerNameTf = new JTextField();
            ownerNameTf.setBounds(150, 80, 100, 30);
            panel.add(ownerNameTf);

            JLabel ownerCnpLabel = new JLabel("owner cnp: ");
            ownerCnpLabel.setBounds(40, 120, 100, 30);
            panel.add(ownerCnpLabel);

            final JTextField ownerCnpTf = new JTextField();
            ownerCnpTf.setBounds(150, 120, 100, 30);
            panel.add(ownerCnpTf);

            JLabel balanceLabel = new JLabel("balance: ");
            balanceLabel.setBounds(40, 160, 100, 30);
            panel.add(balanceLabel);

            final JTextField balanceTf = new JTextField();
            balanceTf.setBounds(150, 160, 100, 30);
            panel.add(balanceTf);
            
            JLabel depositTimeLabel = new JLabel("Deposit time: ");
            depositTimeLabel.setBounds(40, 200, 100, 30);
            panel.add(depositTimeLabel);

            final JTextField depositTimeTf = new JTextField();
            depositTimeTf.setBounds(150, 200, 100, 30);
            panel.add(depositTimeTf);

            JLabel interestLabel = new JLabel("interest: ");
            interestLabel.setBounds(40, 240, 100, 30);
            panel.add(interestLabel);

            final JTextField interestTf = new JTextField();
            interestTf.setBounds(150, 240, 100, 30);
            panel.add(interestTf);



            JButton insertButton = new JButton("INSERT");
            insertButton.setBounds(40, 280, 100, 30);
            panel.add(insertButton);

            insertButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        Person owner = new Person(ownerNameTf.getText(), ownerCnpTf.getText());
                        System.out.println(owner.hashCode());
                        if (!bank.getBankDB().containsKey(owner)) {
                            System.out.println("Wrong person");
                        }
                        assert bank.getBankDB().containsKey(owner) : "There is no person with specified data";
                        SavingsAccount savingsAccount = new SavingsAccount(Integer.parseInt(idTf.getText()), owner, Integer.parseInt(balanceTf.getText()),
                                Integer.parseInt(depositTimeTf.getText()), Integer.parseInt(interestTf.getText()));
                        bank.addSavingsAccount(owner, savingsAccount);

                        adminInterface();
                    } catch (AssertionError er) {
                        errorDisplay(er.getMessage());
                    }

                }
            });

            JButton backButton = new JButton("BACK");
            backButton.setBounds(40, 600, 200, 30);
            panel.add(backButton);

            backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    adminInterface();
                }
            });
            writeDeserializable(bank);
            panel.repaint();
            panel.revalidate();

        }

        public void insertSpendingsAccount(){
            panel.removeAll();
            panel.setLayout(null);

            JLabel idLabel = new JLabel("id: ");
            idLabel.setBounds(40, 40, 100, 30);
            panel.add(idLabel);

            final JTextField idTf = new JTextField();
            idTf.setBounds(150, 40, 100, 30);
            panel.add(idTf);

            JLabel ownerNameLabel = new JLabel("owner name: ");
            ownerNameLabel.setBounds(40, 80, 100, 30);
            panel.add(ownerNameLabel);

            final JTextField ownerNameTf = new JTextField();
            ownerNameTf.setBounds(150, 80, 100, 30);
            panel.add(ownerNameTf);

            JLabel ownerCnpLabel = new JLabel("owner cnp: ");
            ownerCnpLabel.setBounds(40, 120, 100, 30);
            panel.add(ownerCnpLabel);

            final JTextField ownerCnpTf = new JTextField();
            ownerCnpTf.setBounds(150, 120, 100, 30);
            panel.add(ownerCnpTf);

            JLabel balanceLabel = new JLabel("balance: ");
            balanceLabel.setBounds(40, 160, 100, 30);
            panel.add(balanceLabel);

            final JTextField balanceTf = new JTextField();
            balanceTf.setBounds(150, 160, 100, 30);
            panel.add(balanceTf);

            JButton insertButton = new JButton("INSERT");
            insertButton.setBounds(40, 280, 100, 30);
            panel.add(insertButton);

            insertButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        Person owner = new Person(ownerNameTf.getText(), ownerCnpTf.getText());
                        System.out.println(owner.hashCode());
                        if (!bank.getBankDB().containsKey(owner)) {
                            System.out.println("Wrong person");
                        }
                        assert bank.getBankDB().containsKey(owner) : "There is no person with specified data";
                        SpendingAccount spendingAccount = new SpendingAccount(Integer.parseInt(idTf.getText()), owner, Integer.parseInt(balanceTf.getText()),0);
                        bank.addSpendingAccount(owner, spendingAccount);

                        adminInterface();
                    } catch (AssertionError er) {
                        errorDisplay(er.getMessage());
                    }

                }
            });

            JButton backButton = new JButton("BACK");
            backButton.setBounds(40, 600, 200, 30);
            panel.add(backButton);

            backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    adminInterface();
                }
            });
            writeDeserializable(bank);
            panel.repaint();
            panel.revalidate();

        }

        public void showAllAccounts() {
            try {
                String[] columns = {"Id", "Owner", "Balance"};
                JTable jTable = new JTable(bank.viewAccounts(),columns);
                JScrollPane scrollPane = new JScrollPane(jTable);
                scrollPane.setBounds(450,250,400,200);
                panel.add(scrollPane);
            } catch (AssertionError er) {
                errorDisplay(er.getMessage());
            }
            writeDeserializable(bank);
        }

        public void editAccount(){
            panel.removeAll();
            panel.setLayout(null);

            JLabel oldIdLabel = new JLabel(" oldId: ");
            oldIdLabel.setBounds(40, 40, 100, 30);
            panel.add(oldIdLabel);

            final JTextField oldIdTf = new JTextField();
            oldIdTf.setBounds(150, 40, 100, 30);
            panel.add(oldIdTf);

            JLabel newIdLabel = new JLabel("new ID: ");
            newIdLabel.setBounds(40, 80, 100, 30);
            panel.add(newIdLabel);

            final JTextField newIdTf = new JTextField();
            newIdTf.setBounds(150, 80, 100, 30);
            panel.add(newIdTf);

            JLabel newBalanceLabel = new JLabel("new Balance: ");
            newBalanceLabel.setBounds(40, 120, 100, 30);
            panel.add(newBalanceLabel);

            final JTextField newBalanceTf = new JTextField();
            newBalanceTf.setBounds(150, 120, 100, 30);
            panel.add(newBalanceTf);



            JButton editButton = new JButton("edit");
            editButton.setBounds(40, 280, 100, 30);
            panel.add(editButton);

            editButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int ok = 0;
                    int oldId = Integer.parseInt(oldIdTf.getText());

                    Set<Person> persons = bank.getBankDB().keySet();

                    for (Person person: persons) {
                        ArrayList<Account> accounts = bank.getBankDB().get(person);
                        for (Account account : accounts) {
                            if (account.getId() == oldId) {
                                ok =1;
                                bank.editAccount(account, Integer.parseInt(newIdTf.getText()), Integer.parseInt(newBalanceTf.getText()));
                            }
                        }
                    }
                    try {
                        assert !(ok==0) : "There is no such account!!!";
                    } catch (AssertionError err) {
                        errorDisplay(err.getMessage());
                    }

                    adminInterface();
                }
            });

            JButton backButton = new JButton("BACK");
            backButton.setBounds(40, 600, 200, 30);
            panel.add(backButton);

            backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    adminInterface();
                }
            });
            writeDeserializable(bank);
            panel.repaint();
            panel.revalidate();

        }

        public void deleteAccount(){
            panel.removeAll();
            panel.setLayout(null);

            JLabel accountIdLabel = new JLabel("Account Id: ");
            accountIdLabel.setBounds(40, 40, 100, 30);
            panel.add(accountIdLabel);

            final JTextField accountIdTf = new JTextField();
            accountIdTf.setBounds(150, 40, 100, 30);
            panel.add(accountIdTf);


            JButton deleteButton = new JButton("delete");
            deleteButton.setBounds(40, 200, 100, 30);
            panel.add(deleteButton);

            deleteButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int ok=0;
                    Set<Person> persons = bank.getBankDB().keySet();
                    for(Person person: persons) {
                        ArrayList<Account> accounts = bank.getBankDB().get(person);
                        for (Account account : accounts) {
                            if (account.getId() == Integer.parseInt(accountIdTf.getText())){
                                ok=1;
                                accounts.remove(account);
                                bank.getBankDB().put(person, accounts);
                            }
                        }
                    }
                    try {
                        assert !(ok==0) : "There is no such account!!!";
                    } catch (AssertionError err) {
                        errorDisplay(err.getMessage());
                    }
                    adminInterface();
                }
            });

            JButton backButton = new JButton("BACK");
            backButton.setBounds(40, 600, 200, 30);
            panel.add(backButton);

            backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    adminInterface();
                }
            });
            writeDeserializable(bank);
            panel.repaint();
            panel.revalidate();

        }
        
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        public void userInterface(){
            panel.removeAll();
            panel.revalidate();
            panel.setLayout(null);

            JButton depositBt = new JButton("Deposit");
            depositBt.setBounds(40,40, 100, 40);
            panel.add(depositBt);

            JButton withdrawBt = new JButton("Withdraw");
            withdrawBt.setBounds(40,120, 100, 40);
            panel.add(withdrawBt);

            JButton backButton = new JButton("BACK");
            backButton.setBounds(40, 600, 200, 30);
            panel.add(backButton);

            backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    chooseRights();
                }
            });

            depositBt.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    depositInterface();
                }
            });

            withdrawBt.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    withdrawInterface();
                }
            });
            panel.repaint();
            panel.revalidate();
        }
        
        public void depositInterface() {

            panel.removeAll();
            panel.revalidate();
            panel.setLayout(null);

            JLabel accountIdLabel = new JLabel(" Account Id: ");
            accountIdLabel.setBounds(40, 40, 100, 30);
            panel.add(accountIdLabel);

            final JTextField accountIdTf = new JTextField();
            accountIdTf.setBounds(150, 40, 100, 30);
            panel.add(accountIdTf);

            JLabel amountLabel = new JLabel("amount: ");
            amountLabel.setBounds(40, 80, 100, 30);
            panel.add(amountLabel);

            final JTextField amountTf = new JTextField();
            amountTf.setBounds(150, 80, 100, 30);
            panel.add(amountTf);

            JButton goButton = new JButton("Deposit");
            goButton.setBounds(40,130, 100, 40);
            panel.add(goButton);

            goButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    try {
                        bank.deposit(Integer.parseInt(accountIdTf.getText()), Integer.parseInt(amountTf.getText()));
                        chooseRights();
                    } catch (AssertionError ex) {
                        System.out.println("Error");
                        errorDisplay(ex.getMessage());
                    }

                }
            });

            JButton backButton = new JButton("BACK");
            backButton.setBounds(40, 600, 200, 30);
            panel.add(backButton);

            backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    userInterface();
                }
            });
            writeDeserializable(bank);
            panel.repaint();
            panel.revalidate();
        }


        public void withdrawInterface() {

            panel.removeAll();
            panel.revalidate();
            panel.setLayout(null);

            JLabel accountIdLabel = new JLabel(" Account Id: ");
            accountIdLabel.setBounds(40, 40, 100, 30);
            panel.add(accountIdLabel);

            final JTextField accountIdTf = new JTextField();
            accountIdTf.setBounds(150, 40, 100, 30);
            panel.add(accountIdTf);

            JLabel amountLabel = new JLabel("amount: ");
            amountLabel.setBounds(40, 80, 100, 30);
            panel.add(amountLabel);

            final JTextField amountTf = new JTextField();
            amountTf.setBounds(150, 80, 100, 30);
            panel.add(amountTf);

            JButton goButton = new JButton("withdraw");
            goButton.setBounds(40,130, 100, 40);
            panel.add(goButton);

            goButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    try {
                        bank.withdraw(Integer.parseInt(accountIdTf.getText()), Integer.parseInt(amountTf.getText()));
                        chooseRights();
                    } catch (AssertionError ex) {
                        System.out.println("Error");
                        errorDisplay(ex.getMessage());
                    }

                }
            });

            JButton backButton = new JButton("BACK");
            backButton.setBounds(40, 600, 200, 30);
            panel.add(backButton);

            backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    userInterface();
                }
            });
            writeDeserializable(bank);
            panel.repaint();
            panel.revalidate();
        }


    }

