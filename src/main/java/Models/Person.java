package Models;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Person implements Serializable{

    private String name;
    private String cnp;


    public Person(String name, String cnp) {
        this.name = name;
        this.cnp = cnp;
    }

    @Override
    public int hashCode() {
        //return Integer.parseInt(cnp);
        return cnp.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() == this.getClass()) {
            Person object = (Person) obj;
            return (this.cnp.equals(object.getCnp()));
        } else {
            return false;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }
}
