package Models;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Set;

public class SavingsAccount extends Account {

    private int minDepositTime;
    private int interest;
    private Date depositDate;

    public SavingsAccount(int id, Person owner, int balance, int minDepositTime, int interest) {
        super(id, owner, balance);
        this.minDepositTime = minDepositTime;
        this.interest = interest;
    }

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public void setId(int id) {
        super.setId(id);
    }

    @Override
    public Person getOwner() {
        return super.getOwner();
    }

    @Override
    public void setOwner(Person owner) {
        super.setOwner(owner);
    }

    @Override
    public int getBalance() {
        return super.getBalance();
    }

    @Override
    public void setBalance(int balance) {
        super.setBalance(balance);
    }

    public int getMinDepositTime() {
        return minDepositTime;
    }

    public void setMinDepositTime(int minDepositTime) {
        this.minDepositTime = minDepositTime;
    }

    public int getInterest() {
        return interest;
    }

    public void setInterest(int interest) {
        this.interest = interest;
    }

    public Date getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(Date depositDate) {
        this.depositDate = depositDate;
    }

    @Override
    public void deposit(int amount) {
        System.out.println("Savings Deposit");
        assert this.getBalance() == 0 : "Cannot deposit the sum until the balance is 0";
        if (this.getBalance() != 0) {
            System.out.println("Cannot deposit the sum until the balance is 0");
        } else {
            this.setBalance(amount + amount*interest/100);
        }

        assert this.getBalance()== 0 : "Nice! You've just deposited " + amount + " RON!";


    }

    @Override
    public int withdraw(int amount) {
        System.out.println("Savings withdraw");
        assert this.getBalance() >= amount : "Inssuficient funds";
        assert this.getBalance() <= amount : "You must withdraw exactly the account balance because or you will loose the interest!!!";
        int withdrawedSum = amount;
        if (amount == this.getBalance()) {
            this.setBalance(this.getBalance()-amount);
        } else {
            System.out.println("You must withdraw exactly the account balance because or you will loose the interest!!!");
        }

        assert withdrawedSum<0 : "Wait for the device to process the sum...Nice, now you have " + withdrawedSum + " RON cash!!!";
        return withdrawedSum;

    }
}
