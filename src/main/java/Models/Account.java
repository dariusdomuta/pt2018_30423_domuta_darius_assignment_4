package Models;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public abstract class Account implements Serializable{

    private int id;
    private Person owner;
    private int balance;

    public Account(Person owner, int balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public Account(int id, Person owner, int balance) {
        this.id = id;
        this.owner = owner;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public abstract void deposit(int amount);


    public abstract int withdraw(int amount);

}
