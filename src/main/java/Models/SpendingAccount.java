package Models;

public class SpendingAccount extends Account {

    private int noTransactions;

    public SpendingAccount(int id, Person owner, int balance, int noTransactions) {
        super(id, owner, balance);
        this.noTransactions = noTransactions;
    }



    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public void setId(int id) {
        super.setId(id);
    }

    @Override
    public Person getOwner() {
        return super.getOwner();
    }

    @Override
    public void setOwner(Person owner) {
        super.setOwner(owner);
    }

    @Override
    public int getBalance() {
        return super.getBalance();
    }

    @Override
    public void setBalance(int balance) {
        super.setBalance(balance);
    }

    public int getNoTransactions() {
        return noTransactions;
    }

    public void setNoTransactions(int noTransactions) {
        this.noTransactions = noTransactions;
    }

    @Override
    public void deposit(int amount) {
        System.out.println("Spending Deposit");
        assert amount>0 : "You cannot choose a negative number!!!";
        this.setBalance(this.getBalance()+amount);
        assert this.getBalance()< 0 : "Nice! You've just deposited " + amount + " RON!";
    }

    @Override
    public int withdraw(int amount) {
        System.out.println("Spending withdraw");
        assert this.getBalance()>=amount : "Insufficient funds!!! Current balance is " + this.getBalance();
        this.setBalance(this.getBalance()-amount);
        assert amount<0 : "Wait for the device to process the sum...Nice, now you have " + amount + " RON cash!!!";
        return amount;
    }
}
