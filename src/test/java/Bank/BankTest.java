package Bank;

import Models.Account;
import Models.Person;
import Models.SpendingAccount;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class BankTest {

    @Test
    public void addPersonTest() {

        Bank bank = new Bank();
        Person person = new Person("marcu gogu", "1970724324755");
        bank.addPerson(person);
        System.out.println(bank.getBankDB().containsKey(person));
    }

    @Test
    public void addSpendingAccountTest() {

        Bank bank = new Bank();
        Person person = new Person("marcu gogu", "1970724324755");
        bank.getBankDB().put(person, new ArrayList<Account>());
        SpendingAccount spendingAccount = new SpendingAccount(2323, person, 100, 0);
        bank.addSpendingAccount(person, spendingAccount);
        System.out.println(bank.getBankDB().get(person).get(0).getOwner().getName());
    }




    @Test
    public void editPersonTest() {

        Bank bank = new Bank();
        Person person = new Person("marcu gogu", "1970724324755");
        bank.addPerson(person);
        //TODO: FUMEAZA TIGARA MAI INCOLO
        System.out.println(bank.editPerson(person, "marcu boss", "1970724324759").getName());
    }

    @Test
    public void editAccountTest() {

    }
}